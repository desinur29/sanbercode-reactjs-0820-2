import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time : 100,
      date : new Date()
    }
  }


  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentDidUpdate(){
    if(this.state.time === 0){
        this.render = () => null;
    }
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
      
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date : new Date()
    });
  }


  render(){
    return(
      <>
      <div class="body">
      <div class="clock">
        <h1>Sekarang Jam : {this.state.date.toLocaleTimeString()} </h1>
        <h1>hitung mundur : {this.state.time} </h1>
      </div>
      </div>
     
      </>
    )
  }
}


export default Timer;