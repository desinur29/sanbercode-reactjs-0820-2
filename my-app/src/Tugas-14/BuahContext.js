import React, { useState, createContext } from "react";

export const BuahContext = createContext();

export const BuahProvider = props => {
    const [daftarBuah, setdaftarBuah] = useState(null);
    const [idDataBuah, setidDataBuah] = useState(null);
    const [inputNama, setinputNama] = useState("");
    const [inputHarga, setinputHarga] = useState("");
    const [inputBerat, setinputBerat] = useState("");
    const [indexData, setindexData] = useState(-1);

  
    return (
      <BuahContext.Provider value={[daftarBuah, setdaftarBuah, idDataBuah,setidDataBuah, inputNama,setinputNama,
      inputHarga, setinputHarga, inputBerat,setinputBerat, indexData, setindexData]}>
        {props.children}
      </BuahContext.Provider>
    );
  };