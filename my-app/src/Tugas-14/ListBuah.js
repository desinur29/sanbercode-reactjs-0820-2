import React, {useEffect,useContext} from 'react'
import axios from 'axios'
import {BuahContext} from './BuahContext'

const ListBuah = () =>{
    const [daftarBuah, setdaftarBuah, idDataBuah,setidDataBuah, inputNama,setinputNama,
        inputHarga, setinputHarga, inputBerat,setinputBerat, indexData, setindexData] = useContext(BuahContext)

    const editBuah = (event) =>{
        let idBuah = parseInt(event.target.value);
        let buah = daftarBuah.find(el=>el.id === idBuah)
 
        setidDataBuah(buah.id)
        setinputNama(buah.name);
        setinputHarga(buah.price);
        setinputBerat(buah.weight);
        setindexData(buah.index);
     }
 
     const deleteBuah = (event) =>{
         let idBuah = parseInt(event.target.value)
         axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
         .then(res => {
         var newdaftarBuah = daftarBuah.filter(el=> el.id !== idBuah)
         setdaftarBuah(newdaftarBuah)
         setidDataBuah(null)
         setinputNama("")
         setinputHarga(" ")
         setinputBerat("")
         })
     }
         

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
            if(daftarBuah === null){
                setdaftarBuah(res.data);
                console.log(res.data)
            }
                
        })
      },[daftarBuah]);

  return(
      <>
      <div class="body">
      <div class="tabel-daftar">
            <h1>Context</h1>
        <h1>Tabel Daftar Buah</h1>
        <table id="daftar-buah">
    <thead>
        <tr>
            <th>No</th>
            <th>Buah</th>
            <th>Harga</th>
            <th> Berat</th>
            <th colSpan="2">Aksi</th>
        </tr>
    </thead>
    <tbody>
        {daftarBuah !== null && daftarBuah.map((item,index) => {
            return(
                <tr key={item.id}>
                    <td>{index+1}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.weight/1000} kg</td>
                    <td><center><button onClick={editBuah} value={item.id}>Edit</button></center></td>
                    <td><center><button onClick={deleteBuah} value={item.id}>Delete</button></center></td>
                </tr>
            )
        })}
    </tbody>
</table>
<br></br> 
        </div>
        <br></br>
      </div>
        
      </>
   
  )
}

export default ListBuah