import React, {useContext} from 'react'
import axios from 'axios'
import {BuahContext} from './BuahContext'

const FormBuah = () => {

    const [daftarBuah, setdaftarBuah, idDataBuah,setidDataBuah, inputNama,setinputNama,
        inputHarga, setinputHarga, inputBerat,setinputBerat] = useContext(BuahContext)


        const handleChangeNama = (event) =>{
            setinputNama(event.target.value)
        }
    
        const handleChangeHarga = (event) => {
           setinputHarga(event.target.value)
        }
    
        const handleChangeBerat = (event) =>{
            setinputBerat(event.target.value)
        }
    
        const handleSubmit = (event) => {
            event.preventDefault();
                if(idDataBuah === null){
                    axios.post(`http://backendexample.sanbercloud.com/api/fruits`,{ name:inputNama, price:inputHarga, weight:inputBerat})
                    .then(res => {
                        let data = res.data;
                        console.log(data.id);
                        setdaftarBuah([...daftarBuah, {id: data.id, name : data.name, price:data.price , weight:data.weight}])
                        setidDataBuah(null)
                        setinputNama("")
                        setinputHarga(" ")
                        setinputBerat("")   
                        })
                }else{
                    axios.put(` http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`,{ name:inputNama, price:inputHarga, weight:inputBerat})
                    .then(res => {
                        let data = res.data;
                        let newdaftarBuah = daftarBuah.map(el =>{
                            if(el.id === idDataBuah){
                                el.name = data.name
                                el.price = data.price
                                el.weight = data.weight
                            }
                            return el
                            })
                            setdaftarBuah(newdaftarBuah);
                            setidDataBuah(null)
                            setinputNama("")
                            setinputHarga(" ")
                            setinputBerat("") 
                })
            }
        }

    return(
        <>
        <br></br>
        <div class="tabel-daftar">
        <h1>Form Daftar Buah</h1>
                    <div style={{width: "70%", margin: "0 auto", display: "block"}}>
                        <div style={{border: "1px solid #aaa",padding:"20px", marginBottom:"20px"}}>
                        <form style={{width:"80%"}} onSubmit={handleSubmit}>
                            <label style={{float: "left"}}> Nama Buah : </label>
                            <input style={{float: "right"}} required="required" id="nama" type="text" value={inputNama} onChange={handleChangeNama}/>
                            <br/>
                            <br/>
                            <label style={{float: "left"}}> Harga Buah : </label>
                            <input style={{float: "right"}} required="required" id="harga" type="number" value={inputHarga} onChange={handleChangeHarga}/>
                            <br/>
                            <br/>
                            <label style={{float: "left"}}> Berat Buah (gram) : </label>
                            <input style={{float: "right"}} required="required" id="berat" type="number" value={inputBerat} onChange={handleChangeBerat}/>
                            <br/>
                            <br/>
                            <div>
                            <button>submit</button>
                            </div>
                        </form>
                        </div>
                    </div> 
        </div>
        </>
    )
}

export default FormBuah