import React, {useEffect,useContext} from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas9 from '../Tugas-9/tugas9';
import Tugas10 from '../Tugas-10/tugas10';
import Tugas11 from '../Tugas-11/tugas11';
import Tugas12 from '../Tugas-12/tugas12';
import Tugas13 from '../Tugas-13/tugas13';
import Tugas14 from '../Tugas-14/Buah';
import Navbar from './navbar.js';

const Routes = () => {

    

  return (
    <Router>
        
        <Navbar />
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
            {/*Tidak pakai end tag */}
            <Route exact path="/" component={Tugas9}/>
           <Route path="/tugas10">
            <Tugas10 />
           </Route>
            <Route path="/tugas11">
            <Tugas11 />
            </Route>
            <Route path="/tugas12">
            <Tugas12 />
            </Route>
            <Route path="/tugas13">
            <Tugas13 />
            </Route>
            <Route path="/tugas14">
            <Tugas14 />
            </Route>
        </Switch>
        
      
    </Router>
  );
}

export default Routes