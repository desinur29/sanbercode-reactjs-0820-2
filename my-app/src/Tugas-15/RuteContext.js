import React, { useState, createContext } from "react";

export const RuteContext = createContext();

export const RuteProvider = props =>{
    const[colorNav, setcolorNav] = useState(true);

    return(
        <RuteContext.Provider value={[colorNav, setcolorNav]}>
            {props.children}
        </RuteContext.Provider>
    );
};