import React, { useContext } from "react";
import {Link} from "react-router-dom";
import {RuteContext} from './RuteContext'

const Navbar = () =>{

    const [colorNav, setcolorNav] = useContext(RuteContext);

    const changecolor = () =>{
       if (colorNav === true){
           setcolorNav(false);
       } else(
            setcolorNav (true)
       )
    }

    

    return(
        <>
        <header>
           <nav className={colorNav? 'background-light' : 'background-dark'}>  
               <div class="menu">
                <ul>
                    <li><Link to="/">Tugas 9</Link></li>
                    <li><Link to="/Tugas10">Tugas 10</Link></li>
                    <li><Link to="/Tugas11">Tugas 11</Link></li>
                    <li><Link to="/Tugas12">Tugas 12</Link></li>
                    <li><Link to="/Tugas13">Tugas 13</Link></li>
                    <li><Link to="/Tugas14">Tugas 14</Link></li>
                    <li><Link onClick={changecolor}>Tugas 15</Link></li>
                </ul>
               </div>
           </nav>
        </header>
        </>
    )
    
}

export default Navbar