import React, {useState,useEffect} from 'react'
import axios from 'axios'

const HooksApi = () => {
    //Deklasrasi variabel state baru 
    const [daftarBuah, setdaftarBuah] = useState(null);
    const [idDataBuah, setidDataBuah] = useState(null);
    const [inputNama, setinputNama] = useState("");
    const [inputHarga, setinputHarga] = useState("");
    const [inputBerat, setinputBerat] = useState("");
    const [indexData, setindexData] = useState(-1);

    
    

    const handleChangeNama = (event) =>{
        setinputNama(event.target.value)
    }

    const handleChangeHarga = (event) => {
       setinputHarga(event.target.value)
    }

    const handleChangeBerat = (event) =>{
        setinputBerat(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault();
            if(idDataBuah === null){
                axios.post(`http://backendexample.sanbercloud.com/api/fruits`,{ name:inputNama, price:inputHarga, weight:inputBerat})
                .then(res => {
                    let data = res.data;
                    console.log(data.id);
                    setdaftarBuah([...daftarBuah, {id: data.id, name : data.name, price:data.price , weight:data.weight}])
                    setidDataBuah(null)
                    setinputNama("")
                    setinputHarga(" ")
                    setinputBerat("")   
                    })
            }else{
                axios.put(` http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`,{ name:inputNama, price:inputHarga, weight:inputBerat})
                .then(res => {
                    let data = res.data;
                    let newdaftarBuah = daftarBuah.map(el =>{
                        if(el.id === idDataBuah){
                            el.name = data.name
                            el.price = data.price
                            el.weight = data.weight
                        }
                        return el
                        })
                        setdaftarBuah(newdaftarBuah);
                        setidDataBuah(null)
                        setinputNama("")
                        setinputHarga(" ")
                        setinputBerat("") 
            })
        }
    }
        

    const editBuah = (event) =>{
       let idBuah = parseInt(event.target.value);
       let buah = daftarBuah.find(el=>el.id === idBuah)

       setidDataBuah(buah.id)
       setinputNama(buah.name);
       setinputHarga(buah.price);
       setinputBerat(buah.weight);
       setindexData(buah.index);
    }

    const deleteBuah = (event) =>{
        let idBuah = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
        .then(res => {
        var newdaftarBuah = daftarBuah.filter(el=> el.id !== idBuah)
        setdaftarBuah(newdaftarBuah)
        setidDataBuah(null)
        setinputNama("")
        setinputHarga(" ")
        setinputBerat("")
        })
    }
        

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
            if(daftarBuah === null){
                setdaftarBuah(res.data);
                console.log(res.data)
            }
                
        })
      },[daftarBuah]);
    
        return(
           <>
           <div class="body">
           <div class="tabel-daftar">
               <h1>Belajar dengan Hooks,API, dan Axios</h1>
               <h1>Tabel Daftar Buah</h1>
                    <table id="daftar-buah">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Buah</th>
                                <th>Harga</th>
                                <th> Berat</th>
                                <th colSpan="2">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {daftarBuah !== null && daftarBuah.map((item,index) => {
                                return(
                                    <tr key={item.id}>
                                        <td>{index+1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight/1000} kg</td>
                                        <td><center><button onClick={editBuah} value={item.id}>Edit</button></center></td>
                                        <td><center><button onClick={deleteBuah} value={item.id}>Delete</button></center></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    <h1>Form Daftar Buah</h1>
                    <div style={{width: "70%", margin: "0 auto", display: "block"}}>
                        <div style={{border: "1px solid #aaa",padding:"20px",marginBottom:"20px"}}>
                        <form style={{width:"80%"}} onSubmit={handleSubmit}>
                            <label style={{float: "left"}}> Nama Buah : </label>
                            <input style={{float: "right"}} required="required" id="nama" type="text" value={inputNama} onChange={handleChangeNama}/>
                            <br/>
                            <br/>
                            <label style={{float: "left"}}> Harga Buah : </label>
                            <input style={{float: "right"}} required="required" id="harga" type="number" value={inputHarga} onChange={handleChangeHarga}/>
                            <br/>
                            <br/>
                            <label style={{float: "left"}}> Berat Buah (gram) : </label>
                            <input style={{float: "right"}} required="required" id="berat" type="number" value={inputBerat} onChange={handleChangeBerat}/>
                            <br/>
                            <br/>
                            <div>
                            <button>submit</button>
                            </div>
                        </form>
                        </div>
                    </div> 
            </div>

           </div>
           </>
        )
}


export default HooksApi

