import React, {Component} from 'react'
import './tugas12.css'


class Lists extends Component{
    constructor(props){
        super(props)
        this.state = {
            daftarBuah : [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ],
            inputNama : "",
            inputHarga : "",
            inpurBerat : "",
            index : -1
        }

        this.handleChangeNama = this.handleChangeNama.bind(this);
        this.handleChangeHarga = this.handleChangeHarga.bind(this);
        this.handleChangeBerat = this.handleChangeBerat.bind(this)
        this.handleSumbit = this.handleSubmit.bind(this);
    }

    handleChangeNama(event){
        let value = event.target.value
        this.setState({
            inputNama : value
        })
    }

    handleChangeHarga(event){
        let value = event.target.value
        this.setState({
            inputHarga : value
        })
    }

    handleChangeBerat(event){
        let value = event.target.value
        this.setState({
            inputBerat : value
        })
    }

    handleSubmit(event){
        event.preventDefault()
        let index = this.state.index
        
        /*console.log(this.state.inputNama);
        console.log(this.state.inputHarga);
        console.log(this.state.inputBerat);*/
        
        if(index === -1){
            console.log(index)
            let inputBuahBaru = {nama : this.state.inputNama, harga : this.state.inputHarga, berat:this.state.inputBerat}
        //console.log(inputBuahBaru);
            this.setState({
                daftarBuah: [...this.state.daftarBuah, inputBuahBaru],
                inputNama: "",
                inputHarga : "",
                inputBerat: " ",
                index:-1
                })
        }else{
            console.log(index)
            let inputBuahBaru = this.state.daftarBuah
            inputBuahBaru[index] = {
                nama : this.state.inputNama, harga : this.state.inputHarga, berat:this.state.inputBerat
            }
            //inputBuahBaru[index].harga = this.state.inputHarga
           // inputBuahBaru[index].berat= this.state.inputBerat
            //console.log(inputBuahBaru)

            this.setState({
                daftarBuah : [...inputBuahBaru],
                inputNama: "",
                inputHarga : "",
                inputBerat: " ",
                index : -1
            })
                
            }
        }
        

    editBuah = (el) =>{
        console.log(el)
        //let inputBuahBaru = this.state.daftarBuah; // make a separate copy of the array
        var index = this.state.daftarBuah.indexOf(el)
        console.log(index)
        this.setState({
            inputNama : el.nama,
            inputHarga : el.harga,
            inputBerat : el.berat,
            index : index
        })
    }

    deleteBuah = (el) =>{
        let inputBuahBaru = this.state.daftarBuah; // make a separate copy of the array
        var index = inputBuahBaru.indexOf(el)
        console.log(index)
        if (index !== -1) {
        inputBuahBaru.splice(index, 1);
        this.setState({
            daftarBuah : [...inputBuahBaru],
            inputNama: "",
            inputHarga : "",
            inputBerat: " "
        })
        }
        console.log(inputBuahBaru)
    }

    render(){
        return(
           <>
           <div class="body">
           <div class="tabel-daftar-buah">
               <h1>Tabel Daftar Buah</h1>
                    <table id="daftar-buah">
                        <thead>
                            <tr>
                                <th>Buah</th>
                                <th>Harga</th>
                                <th> Berat</th>
                                <th colspan="2">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.daftarBuah.map(el => {
                                return(
                                    <tr>
                                        <td>{el.nama}</td>
                                        <td>{el.harga}</td>
                                        <td>{el.berat/1000} kg</td>
                                        <td><button onClick={() => this.editBuah(el)} >Edit</button></td>
                                        <td><button onClick={() => this.deleteBuah(el)} >Delete</button></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    <form onSubmit={this.handleSumbit}>
                        <table id="tabel-form">
                            <tr>
                                <td>
                                    <label> Masukkan Nama Buah : </label>
                                </td>
                                <td>
                                    <input required="required" id="nama" type="text" value={this.state.inputNama} onChange={this.handleChangeNama}/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label> Masukkan Harga Buah : </label>
                                </td>
                                <td>
                                    <input required="required" id="harga" type="number" value={this.state.inputHarga} onChange={this.handleChangeHarga}/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label> Masukkan Berat Buah (gram) : </label>
                                </td>
                                <td>
                                    <input required="required" id="berat" type="number" value={this.state.inputBerat} onChange={this.handleChangeBerat}/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <button>submit</button>
                                </td>
                            </tr>
                        </table>
                    </form>
            </div>
           </div>
           </>
        )
    }


}

export default Lists

