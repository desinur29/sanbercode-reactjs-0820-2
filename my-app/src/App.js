import React from 'react';
//import logo from './logo.svg';
import './App.css';
import Routes from './Tugas-15/Routes'
import {RuteProvider} from './Tugas-15/RuteContext'

function App() {
  return (
        <RuteProvider>
            <Routes>
            </Routes>
        </RuteProvider>
          
  );
}

export default App;
