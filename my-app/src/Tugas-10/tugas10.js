import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class Nama extends React.Component{
    render(){
        return(<td>{this.props.name}</td>)
    }
}

class Harga extends React.Component{
    render(){
        return(<td>{this.props.harga}</td>)
    }
}

class Berat extends React.Component{
    render(){
        return(<td>{this.props.berat} kg</td>)
    }
}

class DaftarBuah extends React.Component{
    render(){
        return(
           <>
           <div class="body">
           <div class="tabel-daftar">
               <h1>Tabel Daftar Buah</h1>
           <table class="daftarbuah">
           <th>
               Buah
           </th>
           <th>
               Harga
           </th>
           <th>
               Berat
           </th>
            {dataHargaBuah.map(el => {
                return(
                    <tr>
                        <Nama name={el.nama}/>
                        <Harga harga={el.harga}/>
                        <Berat berat={el.berat/1000}/>
                    </tr>
                )
            })}
            </table>
            </div>
           </div>
           
           </>
        )
    }
}

export default DaftarBuah